use cube_conundrum::game::{game, Game};
use cube_conundrum::get_lines;
use cube_conundrum::FileToParse::Input;

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let games: Vec<Game> = lines.map(|line| game(&line).unwrap().1).collect();
    assert_eq!(100, games.len());

    let games_power: Vec<_> = games.into_iter().map(|game| game.power()).collect();

    let result: u32 = games_power.iter().sum();

    println!("{}", result);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn quick_test() {}
}
