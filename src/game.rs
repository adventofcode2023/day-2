use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{digit1, space0};
use nom::combinator::value;
use nom::multi::separated_list1;
use nom::sequence::preceded;
use nom::IResult;
use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Color {
    Red,
    Green,
    Blue,
}

pub type GameSet = HashMap<Color, u32>;

#[derive(Debug)]
pub struct Game {
    pub id: u32,
    sets: Vec<GameSet>,
}

impl Game {
    // region:    --- Part 1

    pub fn is_possbile(&self, red_limit: u32, green_limit: u32, blue_limit: u32) -> bool {
        for set in &self.sets {
            if let Some(red) = set.get(&Color::Red) {
                if *red > red_limit {
                    return false;
                }
            }
            if let Some(green) = set.get(&Color::Green) {
                if *green > green_limit {
                    return false;
                }
            }
            if let Some(blue) = set.get(&Color::Blue) {
                if *blue > blue_limit {
                    return false;
                }
            }
        }
        true
    }

    // endregion: --- Part 1

    // region:    --- Part 2

    fn needed(&self, color: Color) -> u32 {
        self.sets
            .iter()
            .map(|set| match set.get(&color) {
                Some(val) => *val,
                None => 0,
            })
            .max()
            .unwrap_or(0)
    }

    pub fn power(&self) -> u32 {
        let red = self.needed(Color::Red);
        let green = self.needed(Color::Green);
        let blue = self.needed(Color::Blue);

        red * green * blue
    }

    // endregion: --- Part 2
}

// region:    --- Parsing

fn color(input: &str) -> IResult<&str, (Color, u32)> {
    let (input, quantity) = preceded(space0, digit1)(input)?;
    let quantity = quantity.parse().unwrap();

    let (rest, color) = alt((
        value(Color::Red, tag(" red")),
        value(Color::Green, tag(" green")),
        value(Color::Blue, tag(" blue")),
    ))(input)?;

    Ok((rest, (color, quantity)))
}
fn gameset(input: &str) -> IResult<&str, GameSet> {
    let (rest, colors) = separated_list1(tag(","), color)(input)?;

    let mut gameset = GameSet::new();
    for (color, quantity) in colors {
        gameset.insert(color, quantity);
    }

    Ok((rest, gameset))
}

pub fn game(input: &str) -> IResult<&str, Game> {
    let (input, _) = tag("Game ")(input)?;
    let (input, id) = digit1(input)?;
    let id = id.parse().unwrap();
    let (input, _) = tag(":")(input)?;

    let (_, sets) = separated_list1(tag("; "), gameset)(input)?;

    Ok(("", Game { id, sets }))
}

// endregion: --- Parsing
