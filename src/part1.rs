use cube_conundrum::game::{game, Game};
use cube_conundrum::get_lines;
use cube_conundrum::FileToParse::Input;

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let games: Vec<Game> = lines.map(|line| game(&line).unwrap().1).collect();
    assert_eq!(100, games.len());

    let legal_games: Vec<Game> = games
        .into_iter()
        .filter(|game| game.is_possbile(12, 13, 14))
        .collect();
    // dbg!(legal_games);

    let result: u32 = legal_games.iter().map(|game| game.id).sum();

    println!("{}", result);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn quick_test() {
        let fixture = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green";
        let g = game(fixture).unwrap().1;

        println!("{:?}", g);

        assert!(g.is_possbile(12, 13, 14));

        let fixture = "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue";
        let g = game(fixture).unwrap().1;

        assert!(g.is_possbile(12, 13, 14));

        let fixture = "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red";
        let g = game(fixture).unwrap().1;

        assert!(!g.is_possbile(12, 13, 14));

        let fixture = "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red";
        let g = game(fixture).unwrap().1;

        assert!(!g.is_possbile(12, 13, 14));

        let fixture = "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        let g = game(fixture).unwrap().1;

        assert!(g.is_possbile(12, 13, 14));
    }
}
